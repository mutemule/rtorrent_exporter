package rtorrentexporter

import (
	"regexp"
	"strings"
	"testing"
)

func TestTorrentsCollector(t *testing.T) {
	// Info-hashes are usually 40 bytes, but we will make ours only
	// 4 for easier testing
	hashA := strings.Repeat("A", 4)
	hashB := strings.Repeat("B", 4)
	hashC := strings.Repeat("C", 4)

	var tests = []struct {
		desc    string
		ds      *testTorrentsSource
		matches []*regexp.Regexp
	}{
		{
			desc: "one torrent",
			ds: &testTorrentsSource{
				torrents: []string{
					hashA,
				},
				files: map[string]string{
					hashA: "foo",
				},
				rate:  1024,
				total: 1024,
				ratio: 1024,
			},
			matches: []*regexp.Regexp{
				regexp.MustCompile(`rtorrent_torrents 1`),
				regexp.MustCompile(`rtorrent_torrents_started 1`),
				regexp.MustCompile(`rtorrent_torrents_stopped 1`),
				regexp.MustCompile(`rtorrent_torrents_complete 1`),
				regexp.MustCompile(`rtorrent_torrents_incomplete 1`),
				regexp.MustCompile(`rtorrent_torrents_hashing 1`),
				regexp.MustCompile(`rtorrent_torrents_seeding 1`),
				regexp.MustCompile(`rtorrent_torrents_leeching 1`),
				regexp.MustCompile(`rtorrent_torrents_active 1`),

				regexp.MustCompile(`rtorrent_torrents_download_rate_bytes{info_hash="AAAA",name="foo"} 1024`),
				regexp.MustCompile(`rtorrent_torrents_download_total_bytes{info_hash="AAAA",name="foo"} 1024`),
				regexp.MustCompile(`rtorrent_torrents_upload_rate_bytes{info_hash="AAAA",name="foo"} 1024`),
				regexp.MustCompile(`rtorrent_torrents_upload_total_bytes{info_hash="AAAA",name="foo"} 1024`),
				regexp.MustCompile(`rtorrent_torrents_ratio{info_hash="AAAA",name="foo"} 1024`),
			},
		},
		{
			desc: "three torrents",
			ds: &testTorrentsSource{
				torrents: []string{
					hashA,
					hashB,
					hashC,
				},
				files: map[string]string{
					hashA: "foo",
					hashB: "bar",
					hashC: "baz",
				},
				rate:  2048,
				total: 2048,
				ratio: 2048,
			},
			matches: []*regexp.Regexp{
				regexp.MustCompile(`rtorrent_torrents 3`),
				regexp.MustCompile(`rtorrent_torrents_started 3`),
				regexp.MustCompile(`rtorrent_torrents_stopped 3`),
				regexp.MustCompile(`rtorrent_torrents_complete 3`),
				regexp.MustCompile(`rtorrent_torrents_incomplete 3`),
				regexp.MustCompile(`rtorrent_torrents_hashing 3`),
				regexp.MustCompile(`rtorrent_torrents_seeding 3`),
				regexp.MustCompile(`rtorrent_torrents_leeching 3`),
				regexp.MustCompile(`rtorrent_torrents_active 3`),

				regexp.MustCompile(`rtorrent_torrents_download_rate_bytes{info_hash="AAAA",name="foo"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_download_total_bytes{info_hash="AAAA",name="foo"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_upload_rate_bytes{info_hash="AAAA",name="foo"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_upload_total_bytes{info_hash="AAAA",name="foo"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_ratio{info_hash="AAAA",name="foo"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_download_rate_bytes{info_hash="BBBB",name="bar"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_download_total_bytes{info_hash="BBBB",name="bar"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_upload_rate_bytes{info_hash="BBBB",name="bar"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_upload_total_bytes{info_hash="BBBB",name="bar"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_ratio{info_hash="BBBB",name="bar"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_download_rate_bytes{info_hash="CCCC",name="baz"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_download_total_bytes{info_hash="CCCC",name="baz"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_upload_rate_bytes{info_hash="CCCC",name="baz"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_upload_total_bytes{info_hash="CCCC",name="baz"} 2048`),
				regexp.MustCompile(`rtorrent_torrents_ratio{info_hash="CCCC",name="baz"} 2048`),
			},
		},
	}

	for i, tt := range tests {
		t.Logf("[%02d] test %q", i, tt.desc)

		out := testCollector(t, NewTorrentsCollector(tt.ds))

		for j, m := range tt.matches {
			t.Logf("\t[%02d:%02d] match: %s", i, j, m.String())

			if !m.Match(out) {
				t.Fatal("\toutput failed to match regex")
			}
		}
	}
}

var _ TorrentsSource = &testTorrentsSource{}

type testTorrentsSource struct {
	torrents []string
	files    map[string]string
	rate     int
	total    int
	ratio    int
}

func (ds *testTorrentsSource) All() ([]string, error)        { return ds.torrents, nil }
func (ds *testTorrentsSource) Started() ([]string, error)    { return ds.torrents, nil }
func (ds *testTorrentsSource) Stopped() ([]string, error)    { return ds.torrents, nil }
func (ds *testTorrentsSource) Complete() ([]string, error)   { return ds.torrents, nil }
func (ds *testTorrentsSource) Incomplete() ([]string, error) { return ds.torrents, nil }
func (ds *testTorrentsSource) Hashing() ([]string, error)    { return ds.torrents, nil }
func (ds *testTorrentsSource) Seeding() ([]string, error)    { return ds.torrents, nil }
func (ds *testTorrentsSource) Leeching() ([]string, error)   { return ds.torrents, nil }
func (ds *testTorrentsSource) Active() ([]string, error)     { return ds.torrents, nil }

func (ds *testTorrentsSource) BaseFilename(infoHash string) (string, error) {
	return ds.files[infoHash], nil
}
func (ds *testTorrentsSource) DownloadRate(_ string) (int, error)  { return ds.rate, nil }
func (ds *testTorrentsSource) DownloadTotal(_ string) (int, error) { return ds.total, nil }
func (ds *testTorrentsSource) UploadRate(_ string) (int, error)    { return ds.rate, nil }
func (ds *testTorrentsSource) UploadTotal(_ string) (int, error)   { return ds.total, nil }
func (ds *testTorrentsSource) Ratio(_ string) (int, error)         { return ds.ratio, nil }
