module gitlab.com/mutemule/rtorrent_exporter

go 1.22

toolchain go1.22.5

require (
	github.com/prometheus/client_golang v1.19.1
	gitlab.com/mutemule/rtorrent v0.0.0-20240801114839-e5f230f1f022
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/kolo/xmlrpc v0.0.0-20220921171641-a4b6fa1dd06b // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.55.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	golang.org/x/sys v0.22.0 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
)

replace (
	github.com/Sirupsen/logrus v1.8.1 => github.com/sirupsen/logrus v1.8.1
	github.com/circonus-labs/circonusllhist => github.com/openhistogram/circonusllhist v0.3.0
	github.com/coreos/bbolt => go.etcd.io/bbolt v1.3.8
	gopkg.in/yaml.v2 v2.2.1 => gopkg.in/yaml.v2 v2.2.5
	gopkg.in/yaml.v2 v2.2.2 => gopkg.in/yaml.v2 v2.2.5
	gopkg.in/yaml.v2 v2.2.3 => gopkg.in/yaml.v2 v2.2.5
	gopkg.in/yaml.v2 v2.2.4 => gopkg.in/yaml.v2 v2.2.5
)
