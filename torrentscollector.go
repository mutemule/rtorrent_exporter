package rtorrentexporter

import (
	"log"

	"gitlab.com/mutemule/rtorrent"
	"github.com/prometheus/client_golang/prometheus"
)

var _ TorrentsSource = &rtorrent.DownloadService{}

// A TorrentsSource is a type which can retrieve torrent information from
// rTorrent.  It is implemented by *rtorrent.DownloadService.
type TorrentsSource interface {
	All() ([]string, error)
	Started() ([]string, error)
	Stopped() ([]string, error)
	Complete() ([]string, error)
	Incomplete() ([]string, error)
	Hashing() ([]string, error)
	Seeding() ([]string, error)
	Leeching() ([]string, error)
	Active() ([]string, error)

	BaseFilename(infoHash string) (string, error)
	DownloadRate(infoHash string) (int, error)
	DownloadTotal(infoHash string) (int, error)
	UploadRate(infoHash string) (int, error)
	UploadTotal(infoHash string) (int, error)
	Ratio(infoHash string) (int, error)
}

// A TorrentsCollector is a Prometheus collector for metrics regarding rTorrent
type TorrentsCollector struct {
	Torrents           *prometheus.Desc
	TorrentsStarted    *prometheus.Desc
	TorrentsStopped    *prometheus.Desc
	TorrentsComplete   *prometheus.Desc
	TorrentsIncomplete *prometheus.Desc
	TorrentsHashing    *prometheus.Desc
	TorrentsSeeding    *prometheus.Desc
	TorrentsLeeching   *prometheus.Desc
	TorrentsActive     *prometheus.Desc

	DownloadRateBytes  *prometheus.Desc
	DownloadTotalBytes *prometheus.Desc
	UploadRateBytes    *prometheus.Desc
	UploadTotalBytes   *prometheus.Desc
	Ratio              *prometheus.Desc

	ds TorrentsSource
}

// Verify that TorrentsCollector implements the prometheus.Collector interface.
var _ prometheus.Collector = &TorrentsCollector{}

// NewTorrentsCollector creates a new TorrentsCollector which collects metrics
// regarding rTorrent
func NewTorrentsCollector(ds TorrentsSource) *TorrentsCollector {
	const (
		subsystem = "torrents"
	)

	var (
		labels = []string{"info_hash", "name"}
	)

	return &TorrentsCollector{
		Torrents: prometheus.NewDesc(
			// Subsystem is used as name so we get "rtorrent_torrents"
			prometheus.BuildFQName(namespace, "", subsystem),
			"Total number of torrents.",
			nil,
			nil,
		),

		TorrentsStarted: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "started"),
			"Number of started torrents.",
			nil,
			nil,
		),

		TorrentsStopped: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "stopped"),
			"Number of stopped torrents.",
			nil,
			nil,
		),

		TorrentsComplete: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "complete"),
			"Number of complete torrents.",
			nil,
			nil,
		),

		TorrentsIncomplete: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "incomplete"),
			"Number of incomplete torrents.",
			nil,
			nil,
		),

		TorrentsHashing: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "hashing"),
			"Number of hashing torrents.",
			nil,
			nil,
		),

		TorrentsSeeding: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "seeding"),
			"Number of seeding torrents.",
			nil,
			nil,
		),

		TorrentsLeeching: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "leeching"),
			"Number of leeching torrents.",
			nil,
			nil,
		),

		TorrentsActive: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "active"),
			"Number of active torrents.",
			nil,
			nil,
		),

		DownloadRateBytes: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "download_rate_bytes"),
			"Current download rate in bytes.",
			labels,
			nil,
		),

		DownloadTotalBytes: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "download_total_bytes"),
			"Total Bytes downloaded.",
			labels,
			nil,
		),

		UploadRateBytes: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "upload_rate_bytes"),
			"Current upload rate in bytes.",
			labels,
			nil,
		),

		UploadTotalBytes: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "upload_total_bytes"),
			"Total Bytes uploaded.",
			labels,
			nil,
		),

		Ratio: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, subsystem, "ratio"),
			"Current upload/download ratio.",
			labels,
			nil,
		),

		ds: ds,
	}
}

// collect begins a metrics collection task for all metrics related to rTorrent
func (c *TorrentsCollector) collect(ch chan<- prometheus.Metric) (*prometheus.Desc, error) {
	if desc, err := c.collectTorrentCounts(ch); err != nil {
		return desc, err
	}

	if desc, err := c.collectActiveTorrents(ch); err != nil {
		return desc, err
	}

	return nil, nil
}

// collectTorrentCounts collects metrics which track number of torrents in
// various possible states.
func (c *TorrentsCollector) collectTorrentCounts(ch chan<- prometheus.Metric) (*prometheus.Desc, error) {
	all, err := c.ds.All()
	if err != nil {
		return c.Torrents, err
	}

	started, err := c.ds.Started()
	if err != nil {
		return c.TorrentsStarted, err
	}

	stopped, err := c.ds.Stopped()
	if err != nil {
		return c.TorrentsStopped, err
	}

	complete, err := c.ds.Complete()
	if err != nil {
		return c.TorrentsComplete, err
	}

	incomplete, err := c.ds.Incomplete()
	if err != nil {
		return c.TorrentsIncomplete, err
	}

	hashing, err := c.ds.Hashing()
	if err != nil {
		return c.TorrentsHashing, err
	}

	seeding, err := c.ds.Seeding()
	if err != nil {
		return c.TorrentsSeeding, err
	}

	leeching, err := c.ds.Leeching()
	if err != nil {
		return c.TorrentsLeeching, err
	}

	ch <- prometheus.MustNewConstMetric(
		c.Torrents,
		prometheus.GaugeValue,
		float64(len(all)),
	)

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsStarted,
		prometheus.GaugeValue,
		float64(len(started)),
	)

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsStopped,
		prometheus.GaugeValue,
		float64(len(stopped)),
	)

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsComplete,
		prometheus.GaugeValue,
		float64(len(complete)),
	)

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsIncomplete,
		prometheus.GaugeValue,
		float64(len(incomplete)),
	)

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsHashing,
		prometheus.GaugeValue,
		float64(len(hashing)),
	)

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsSeeding,
		prometheus.GaugeValue,
		float64(len(seeding)),
	)

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsLeeching,
		prometheus.GaugeValue,
		float64(len(leeching)),
	)

	return nil, nil
}

// collectActiveTorrents collects information about active torrents,
// which are uploading and/or downloading data.
func (c *TorrentsCollector) collectActiveTorrents(ch chan<- prometheus.Metric) (*prometheus.Desc, error) {
	active, err := c.ds.Active()
	if err != nil {
		return c.TorrentsActive, err
	}

	ch <- prometheus.MustNewConstMetric(
		c.TorrentsActive,
		prometheus.GaugeValue,
		float64(len(active)),
	)

	for _, a := range active {
		name, err := c.ds.BaseFilename(a)
		if err != nil {
			return c.DownloadRateBytes, err
		}

		labels := []string{
			a,
			name,
		}

		down, err := c.ds.DownloadRate(a)
		if err != nil {
			return c.DownloadRateBytes, err
		}

		downTotal, err := c.ds.DownloadTotal(a)
		if err != nil {
			return c.DownloadTotalBytes, err
		}

		up, err := c.ds.UploadRate(a)
		if err != nil {
			return c.UploadRateBytes, err
		}

		upTotal, err := c.ds.UploadTotal(a)
		if err != nil {
			return c.UploadTotalBytes, err
		}

		ratio, err := c.ds.Ratio(a)
		if err != nil {
			return c.Ratio, err
		}

		ch <- prometheus.MustNewConstMetric(
			c.DownloadRateBytes,
			prometheus.GaugeValue,
			float64(down),
			labels...,
		)

		ch <- prometheus.MustNewConstMetric(
			c.DownloadTotalBytes,
			prometheus.GaugeValue,
			float64(downTotal),
			labels...,
		)

		ch <- prometheus.MustNewConstMetric(
			c.UploadRateBytes,
			prometheus.GaugeValue,
			float64(up),
			labels...,
		)

		ch <- prometheus.MustNewConstMetric(
			c.UploadTotalBytes,
			prometheus.GaugeValue,
			float64(upTotal),
			labels...,
		)

		ch <- prometheus.MustNewConstMetric(
			c.Ratio,
			prometheus.GaugeValue,
			float64(ratio),
			labels...,
		)
	}

	return nil, nil
}

// Describe sends the descriptors of each metric over to the provided channel.
// The corresponding metric values are sent separately.
func (c *TorrentsCollector) Describe(ch chan<- *prometheus.Desc) {
	ds := []*prometheus.Desc{
		c.Torrents,
		c.TorrentsStarted,
		c.TorrentsStopped,
		c.TorrentsComplete,
		c.TorrentsIncomplete,
		c.TorrentsHashing,
		c.TorrentsSeeding,
		c.TorrentsLeeching,
		c.TorrentsActive,

		c.DownloadRateBytes,
		c.DownloadTotalBytes,
		c.UploadRateBytes,
		c.UploadTotalBytes,
		c.Ratio,
	}

	for _, d := range ds {
		ch <- d
	}
}

// Collect sends the metric values for each metric pertaining to the rTorrent
// torrents to the provided prometheus Metric channel.
func (c *TorrentsCollector) Collect(ch chan<- prometheus.Metric) {
	if desc, err := c.collect(ch); err != nil {
		log.Printf("[ERROR] failed collecting torrent metric %v: %v", desc, err)
		ch <- prometheus.NewInvalidMetric(desc, err)
		return
	}
}
