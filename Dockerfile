FROM docker.io/library/golang:1.22-alpine as builder

ARG version="development"
ENV CGO_ENABLED=0

RUN apk update && \
    apk upgrade && \
    apk add binutils ca-certificates git tzdata

WORKDIR /go/src/rtorrent_exporter
COPY . .
RUN go install -ldflags="-X 'main.Version=${version}'" -v ./... && \
    strip /go/bin/rtorrent_exporter

RUN adduser -H -h / -D -g "rtorrent_exporter" rtorrent_exporter
RUN update-ca-certificates


FROM scratch

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs /etc/ssl/certs
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /go/bin/rtorrent_exporter /rtorrent_exporter

ENTRYPOINT ["/rtorrent_exporter"]
